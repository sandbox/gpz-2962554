<?php

namespace Drupal\entity_to_config;

/**
 * Interface EntityToConfigServiceInterface.
 */
interface EntityToConfigServiceInterface {

  public function convertFromEntity($entity, $config_name = "");
  public function convert($entity_type_id, $bundle, $eid);
}
