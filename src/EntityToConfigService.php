<?php

namespace Drupal\entity_to_config;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\field\Entity\FieldConfig;

/**
 * Class EntityToConfigService.
 */
class EntityToConfigService implements EntityToConfigServiceInterface {

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;


  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Only custom field OR all fields ?
   *
   * @var bool
   */
  protected $customFieldsOnly;

  /**
   * Add information about referenced entities
   *
   * @var bool
   */
  protected $entityRefInfo;


  /**
   * Constructs a new EntityToConfigService object.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityFieldManagerInterface $entity_field_manager, EntityTypeManagerInterface $entity_type_manager) {
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;

    $this->customFieldsOnly = TRUE;
    $this->entityRefInfo = TRUE;
  }


  /**
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity to convert into a configuration
   * @param string $config_name
   *   [optional] Name of the output configuration
   */
  public function convertFromEntity($entity, $config_name = "") {
    $fields = $entity->getFieldDefinitions();

    $result = [];
    $this->convertFields($entity, $fields, $result, FALSE);

    // Create/Update config
    $this->generateConf($result, $config_name ? : ($entity->getEntityTypeId() . '.' . $entity->bundle() . '.' . $entity->id()));
  }

  public function convert($entity_type_id, $bundle, $eid) {
    $fields = $this->entityFieldManager->getFieldDefinitions($entity_type_id, $bundle);
    $storage = $this->entityTypeManager->getStorage($entity_type_id);
    $conf = $storage->load($eid);

    $result = [];
    // 'Parse' entity and create a result table
    $this->convertFields($conf, $fields, $result, FALSE);

    // Create/Update config
    $this->generateConf($result, $entity_type_id . '.' . $bundle . '.' . $eid);
  }

  protected function convertFields($entity2, $fieldsDefs, &$res, $entity_ref_info) {

    // Add information about the entity
    if ($entity_ref_info) {
      if ($entity2 instanceof ContentEntityInterface) {
        /** @var \Drupal\core\Entity\ContentEntityInterface $entity2 */
        $res['e2c_type_id'] = $entity2->getEntityTypeId();
        $res['e2c_bundle'] = $entity2->bundle();
        $res['e2c_id'] = $entity2->id();
      }
    }

    foreach ($fieldsDefs as $fieldName => $fieldDef) {
      $cardinality = $fieldDef->getFieldStorageDefinition()->getCardinality();

      if ($this->customFieldsOnly && !($fieldDef instanceof FieldConfig))
        continue;

      if (method_exists($entity2->get($fieldName), 'referencedEntities')) {
        $refs = $entity2->get($fieldName)->referencedEntities();
        foreach ($refs as $idx => $entity) {
          if (!isset($res[$fieldName]))
            $res[$fieldName] = [];

          //if ($entity instanceof \Drupal\paragraphs\Entity\Paragraph) {
          if (method_exists($entity, 'getFieldDefinitions')) {
            $res[$fieldName][$idx] = [];

            // Do the same for the referenced entities
            $this->convertFields($entity, $entity->getFieldDefinitions(), $res[$fieldName][$idx], $this->entityRefInfo);
          }
          else {
            if ($cardinality === 1)
              $res[$fieldName]['target_id'] = $entity->id();
            else
              $res[$fieldName][$idx]['target_id'] = $entity->id();
          }
        }
      }
      else {
        if ($cardinality === 1)
          $res[$fieldName] = $entity2->get($fieldName)->value;//->getValue();
        else {
          $elements =  $entity2->get($fieldName)->getValue();
          foreach ( $elements as $idx => $element ) {
            $res[$fieldName][$idx] = $element['value'];
          }
        }
      }
    }
  }

  protected function generateConf($data, $conf_name) {
    // Convert to conf
    try {
      // Services
      $configFactory = \Drupal::configFactory();
      //$uuid_service = \Drupal::service('uuid');
      //$data['uuid'] = $uuid_service->generate();

      $configFactory->getEditable($conf_name)->setData($data)->save();
    }
    catch (\Exception $e) {
      dump($e->getMessage());
    }
  }

  protected function generateYAMLFile($data, $conf_name) {
    // Generate YAML file
    $yamlDump = \Symfony\Component\Yaml\Yaml::dump($data);
    file_unmanaged_save_data($yamlDump, $conf_name . ".yml", FILE_EXISTS_REPLACE);
  }
}
